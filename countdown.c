///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04a - Countdown
//
// Usage:  countdown
//
// Result:
//   Counts down (or towards) a significant date
//
// Example:
//   @todo
//
// @author  Jeraldine Milla <millaje@hawaii.edu>
// @date    6 Feb 2021 
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

struct tm referT;
int main() {
   time_t refT, curT;
   long diff_y, diff_d, diff_h, diff_m, diff_s;

   referT.tm_year = (2021 - 1999);
   referT.tm_mon = (10 - 1);
   referT.tm_mday = 20;
   referT.tm_hour = 9;
   referT.tm_min = 8;
   referT.tm_sec = 7;
   

   
   refT = mktime(&referT);
   //char* date = asctime(&referT);
   //strftime(buffer,80,"%a %b %d %I:%M:%S %p %Z %Y",timeinfo);

   printf("Reference time: %s\n\n", asctime(&referT));
 
   mktime(&referT);
   while(1){
      curT = time(NULL);
      diff_s = difftime(refT, curT);
   //int secs = countTime;
      if(diff_s <0)
         diff_s = diff_s*(-1);
         //diff_s+=1;
      diff_y = diff_s/(31536000);
      diff_s = diff_s - (diff_y*31536000);
      diff_d = diff_s/ 86400;
      diff_s = diff_s - (diff_d*86400);
      diff_h = diff_s/3600;
      diff_s = diff_s - (diff_h*3600);
      diff_m = diff_s / 60;
      diff_s = diff_s - (diff_m*60);

   printf("Years: %d \tDays: %d \tHours: %d \tMinutes: %d \tSeconds: %d\n", diff_y, diff_d, diff_h, diff_m, diff_s);
      sleep(1);
   }
   return 0;
}



//time_t currentTime;
   //struct tm * timeinfo;
   //time(&currentTime);
   //timeinfo = localtime(&currentTime);
   //char* day = asctime(timeinfo);  
   //char buffer[60];

/*
   time_t cTime, cMktime;
   //time_t nTime, nMktime;
   struct tm * cinfo;
   //struct tm * ninfo;
   time(&cTime);
   //time(&nTime);
   cinfo = localtime(&cTime);
   //ninfo = localtime(&nTime);
   cMktime = mktime(cinfo);
   //nMktime = mktime(ninfo);
   //double countTime = difftime(nMktime, cMktime);
*/
